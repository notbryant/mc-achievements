/*		DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2016 Jon Petraglia <jon@qweex.com>

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
*/


#SingleInstance off
#NoTrayIcon


fetchJson(site) {
	local fileurl, tempfile
	fileurl := "ftp://" . RegExReplace(site["user"], "@", "%40") . ":" . site["pass"] . "@" . site["host"] . ":" . site["port"] . "/" . site["dir"] . "/stats/" . site["uuid"] . ".json"
	tempfile := A_Temp . "\" . site["uuid"] . ".json"
	UrlDownloadToFile, % fileurl, % tempfile
	return readFile(tempfile)
}

readFile(file) {
	local jsoncontents
	FileRead, jsoncontents, % file
	jsoncontents := json_toobj(jsoncontents)
	return jsoncontents
}

readMCData() {
	local jsoncontents
	FileRead, jsoncontents, mcdata.json
	return json_toobj(jsoncontents)
}

populate(data) {
	local achievement, biomeList, tvId, acIndex
	Loop, % achievements.MaxIndex() {
		achievement := achievements[A_Index]
		if(achievement = "exploreAllBiomes") {
			tvId := TV_Add(achievement, "", "Icon" . (data["achievement.exploreAllBiomes"]["value"]>0 ? 1 : 2))
			acquiredBiomes := data["achievement.exploreAllBiomes"]["progress"]
			bubbleSort(acquiredBiomes)
			acIndex := 1
			Loop, % biomes.MaxIndex() {
				TV_Add(biomes[A_Index], tvId, "Icon" . (acquiredBiomes[acIndex] = biomes[A_Index] ? 1 : 2))
				if(acquiredBiomes[acIndex] < biomes[A_Index+1])
					acIndex := acIndex + 1
			}
		} else {
			TV_Add(achievement, "", "Icon" . (data["achievement." . achievement] ? 1 : 2))
		}

	}
}

bubbleSort(arr) {
	local temp
	hasChanged = 1
  	size := arr.MaxIndex()
  	while hasChanged {
  		hasChanged = 0
  		Loop, % (size - 1) {
			j := A_Index + 1
			if(arr[j] < arr[A_Index]) {
				temp := arr[A_Index]
				arr[A_Index] := arr[j]
				arr[j] := temp
				hasChanged := 1
			}
  		}
  	}
}


addTab(name, site) {
	global
	GuiControl,,Tab,% name
	tabCount += 1
	Gui, Tab, %tabCount%
	GuiControlGet, tabPos, pos, Tab
	Gui, Add, TreeView,% "imageList" . ImageListID . " vtreeView" . tabCount . " x15 y40 w" . (tabPosW-15) . " h" . (tabPosH-40)
	Gui, Treeview, "vtreeView" . tabCount
	if site["folder"]
		site := readFile(site["folder"] . "/" . site["dir"] . "/stats/" . site["uuid"] . ".json")
	else
		site := fetchJson(site)
	populate(site)
}

Gui, +resize +minsize
Gui, +LastFound
WIN_ID:=WinExist()
Gui, Add, Tab2, w600 h400 x10 y10 vTab -wrap 
tabCount := 0
ImageListID := IL_Create(2)
if(A_IsCompiled)
{
	IL_Add(ImageListID,A_ScriptName,3)
	IL_Add(ImageListID,A_ScriptName,4)
} else {
	IL_Add(ImageListID,"checkmark.ico")
	IL_Add(ImageListID,"xmark.ico")
}

Gui, Show,, Minecraft Achievements

checkData := readMCData()
achievements := checkData["achievements"]
biomes := checkData["biomes"]
bubbleSort(biomes)


IniRead, OutputVarSectionNames, config.ini
Loop, Parse, OutputVarSectionNames, `n
{
	siteConfig := Object()
	configNames := Array("user", "pass", "host", "port", "uuid", "dir")
	for i, name in configNames
	{
		IniRead, derp, config.ini,% A_LoopField, % name
		siteConfig[name] := derp
	}
	addTab(A_LoopField, siteConfig)
}



Return

GuiClose:
ExitApp

Guisize:
	Loop, % tabCount
		AdjustResize("treeView" . A_Index, "wh")
	AdjustResize("Tab","wh")
	Winset, redraw,,ahk_id %WIN_ID%
Return

#Include JSON_ToObj.ahk
#Include AdjustResize.ahk